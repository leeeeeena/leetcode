from typing import List

class Solution:
    def orderOfLargestPlusSign(self, n: int, mines: List[List[int]]) -> int:
        dp = [[n] * n for _ in range(n)]
        banned = set(map(tuple, mines))
        for i in range(n):
            # left
            count = 0
            for j in range(n):
                count = 0 if (i, j) in banned else count + 1
                dp[i][j] = min(dp[i][j], count)
            # right
            count = 0
            for j in range(n - 1, -1, -1):
                count = 0 if (i, j) in banned else count + 1
                dp[i][j] = min(dp[i][j], count)
        for j in range(n):
            # up
            count = 0
            for i in range(n):
                count = 0 if (i, j) in banned else count + 1
                dp[i][j] = min(dp[i][j], count)
            # down
            count = 0
            for i in range(n - 1, -1, -1):
                count = 0 if (i, j) in banned else count + 1
                dp[i][j] = min(dp[i][j], count)
        # print(dp)
        # t = map(max, dp)
        '''
        max(map(max, dp))的使用，因为max函数无法直接作用到矩阵 dp 上，
        通过一个 map 映射，将max函数映射到dp元素上，得到一个迭代对象，再外套一个max函数，
        得到矩阵的最大值，使用方法很巧妙，值得借鉴（也可采用np.max()函数进行直接矩阵最大值求解）
        '''
        return max(map(max, dp))


if __name__ == '__main__':
    n = 5
    mines = [[4, 2]]
    obj = Solution()
    res = obj.orderOfLargestPlusSign(n,mines)
    print(res)
