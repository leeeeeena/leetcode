package doubleContest;

public class doubleContest93_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		doubleContest93_1 obj = new doubleContest93_1();
		String[] strs = {"alic3","bob","3","4","00000"};
		System.out.println(obj.maximumValue(strs));

	}
    public int maximumValue(String[] strs) {
        int n = strs.length;
        int max = 0;
        for(int i=0;i<n;i++){
            String cur = strs[i];
            int len = cur.length();
            int num = 0;
            for(int j=0;j<len;j++){
                if(cur.charAt(j)>='0' && cur.charAt(j)<='9'){
                    num = num*10+(cur.charAt(j)-'0');
                }else{
                    num = -1;
                    break;
                }
            }
            if(num == -1){
                max = Math.max(max,len);
            }else{
                max = Math.max(max,num);
            }
        }
        return max;
    }
}
