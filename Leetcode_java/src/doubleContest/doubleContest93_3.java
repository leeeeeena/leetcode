package doubleContest;

public class doubleContest93_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		doubleContest93_3 obj = new doubleContest93_3();
		int[] stones = {0,2,5,6,7};
		System.out.println(obj.maxJump(stones));
	}
    public int maxJump(int[] stones) {
        int ans = stones[1] - stones[0];
        for(int i=2;i<stones.length;i++){
            ans = Math.max(ans, stones[i] - stones[i - 2]);
        }
        return ans;
    }
}
