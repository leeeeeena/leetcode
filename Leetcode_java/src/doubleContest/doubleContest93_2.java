package doubleContest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class doubleContest93_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		doubleContest93_2 obj = new doubleContest93_2();
		int[] vals = {1,2,3,4,10,-10,-20};
		int[][] edges = {{0,1},{1,2},{1,3},{3,4},{3,5},{3,6}};
		int k = 2;
		System.out.println(obj.maxStarSum(vals, edges, k));
	}
    public int maxStarSum(int[] vals, int[][] edges, int k) {
    	int n=vals.length;
        List<Integer>[]list=new ArrayList[n];
        for(int i=0;i<n;i++)list[i]=new ArrayList<Integer>();
        for(int[]e:edges){//构建无向图
            list[e[0]].add(e[1]);
            list[e[1]].add(e[0]);
        }
        int max=-10001;
        for(int i=0;i<n;i++){
            Collections.sort(list[i],(a,b)->vals[b]-vals[a]);//list的自定义排序
            int temp=vals[i];
            max=Math.max(max,temp);
            for(int j=0;j<Math.min(k,list[i].size());j++){
                temp+=vals[list[i].get(j)];
                max=Math.max(max,temp);
            }
        }
        return max;
    }
}
