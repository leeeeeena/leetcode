package december22;

public class Leetcode1827 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1827 obj = new Leetcode1827();
		int[] nums = {1,1,1};
		System.out.println(obj.minOperations(nums));
	}
    public int minOperations(int[] nums) {
    	int n = nums.length;
    	int sum = 0;
    	for(int i=1;i<n;i++) {
    		if(nums[i]>nums[i-1]) {
    			continue;
    		}else {
    			sum+=nums[i-1]+1-nums[i];
    			nums[i] = nums[i-1]+1;
    		}
    	}
    	return sum;
    }
}
