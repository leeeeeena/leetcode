package december22;

public class Leetcode1780 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1780 obj = new Leetcode1780();
		int n = 21;
		System.out.println(obj.checkPowersOfThree(n));
	}
	public boolean checkPowersOfThree(int n) {
        while (n != 0) {
            if (n % 3 == 2) {
                return false;
            }
            n /= 3;
        }
        return true;
    }

}
