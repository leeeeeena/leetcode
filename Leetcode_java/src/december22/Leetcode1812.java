package december22;

public class Leetcode1812 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1812 obj = new Leetcode1812();
		String coordinates = "a1";
		System.out.println(obj.squareIsWhite(coordinates));
	}
    public boolean squareIsWhite(String coordinates) {
        return ((coordinates.charAt(0) - 'a' + 1) + (coordinates.charAt(1) - '0')) % 2 == 1;
    }
}
