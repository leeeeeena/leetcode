package contest;

import java.util.ArrayList;
import java.util.List;

public class contest320_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest320_3 obj = new contest320_3();
//		int[][] roads = {{3,1},{3,2},{1,0},{0,4},{0,5},{4,6}};
//		int seats = 2;
		int[][] roads = {{0,1},{0,2},{0,3}};
		int seats = 5;
		long res = obj.minimumFuelCost(roads, seats);
		System.out.println(res);
	}
	long ans = 0;
	
    public long minimumFuelCost(int[][] roads, int seats) {
    	 int n = roads.length;
    	 List<Integer>[] lists = new ArrayList[n+1];
    	 for(int i=0;i<=n;i++) {
    		 lists[i] = new ArrayList<Integer>();
    	 }
    	 for(int i=0;i<n;i++) {
    		 int num1 = roads[i][0];
    		 int num2 = roads[i][1];
    		 lists[num1].add(num2);
    		 lists[num2].add(num1);
    	 }
    	 dfs(0,-1,seats,lists);
    	 return ans;
    }
    public long dfs(int cur,int fa,int seats,List<Integer>[] lists) {
    	long size = 1;
    	List<Integer> nextNodes = lists[cur];
    	for(Integer nextNode:nextNodes) {
    		if(nextNode!=fa) {
    			size+=dfs(nextNode,cur,seats,lists);
    		}
    	}
    	if(cur!=0) {
    		ans+=(size+seats-1)/seats;
    	}
    	return size;
    }
}
