package contest;

import java.util.Arrays;

public class contest294_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest294_3 o = new contest294_3();
		int[][] stockPrices = 
		{{1,1},{500000000,499999999},{1000000000,999999998}};
		int res = o.minimumLines(stockPrices);
		System.out.println(res);
	}
    public int minimumLines(int[][] stockPrices) {
    	Arrays.sort(stockPrices, (int[] a, int[] b) -> {
            if (a[0] != b[0]) {
                // 第一个数不相等 第一个数升序
                return a[0] - b[0];
            } else {
                // 第一个数相等 第二个数升序
                return a[1] - b[1];
            }
        });
    	int n = stockPrices.length;
        if(n==1){
            return 0;
        }
    	// for(int i=0;i<n;i++) {
    	// 	System.out.print(stockPrices[i]+" ");
    	// }
    	// System.out.println();
    	int sum = 1;
//    	double prex = stockPrices[1][0]-stockPrices[0][0];
//    	这里换成double有精度问题
    	int prex = stockPrices[1][0]-stockPrices[0][0];
    	int prey = stockPrices[1][1]-stockPrices[0][1];
    	for(int i=2;i<n;i++) {
    		int curx = stockPrices[i][0]-stockPrices[i-1][0];
    		int cury = stockPrices[i][1]-stockPrices[i-1][1];
    		System.out.println(curx*prey);
    		System.out.println(cury*prex);
    		if(curx*prey!=cury*prex) {
    			sum+=1;
    			prex = curx;
    			prey = cury;
    		}
    	}
    	return sum;
    }
}
