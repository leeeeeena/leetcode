package contest;

public class contest317_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest317_1 obj = new contest317_1();
		int[] nums = {1,3,6,10,12,15};
		int res = obj.averageValue(nums);
		System.out.println(res);
	}
    public int averageValue(int[] nums) {
    	int n = nums.length;
    	int sum = 0;
    	int cnt = 0;
    	for(int i=0;i<n;i++) {
    		if(nums[i]%6==0) {
    			sum+=nums[i];
    			cnt++;
    		}
    	}
    	return cnt>0?(sum/cnt):0;
    }
}
