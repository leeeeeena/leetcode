package contest;

import java.util.Arrays;

public class contest323_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest323_1 obj = new contest323_1();
		int[][] grid = {{1,2,4},{3,3,1}};
		System.out.println(obj.deleteGreatestValue(grid));
	}
	public int deleteGreatestValue(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        int sum = 0;
        for(int i=0;i<row;i++){
            Arrays.sort(grid[i]);
        }
        for(int j=0;j<col;j++){
            int colMax = 0;
            for(int i=0;i<row;i++){
                colMax = Math.max(grid[i][j],colMax);
            }
            sum+=colMax;
        }
        return sum;
    }
}
