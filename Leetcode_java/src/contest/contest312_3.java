package contest;

import java.util.ArrayList;
import java.util.List;

public class contest312_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest312_3 obj = new contest312_3();
//		int[] nums = {2,1,1,1,3,4,1};
//		int k = 2;
		
		int[] nums = {878724,201541,179099,98437,35765,327555,475851,598885,849470,943442};
		int k = 4;
		List<Integer> res = obj.goodIndices(nums, k);
	}
    public List<Integer> goodIndices(int[] nums, int k) {
    	int n = nums.length;
    	int[] decrease = new int[n];
    	int[] increase = new int[n];
    	
//    	下标 i 之前 的 k 个元素是 非递增的
    	for(int i=1;i<n;i++) {
    		if(nums[i]<=nums[i-1]) {
    			decrease[i]=decrease[i-1]+1;
    		}else {
    			decrease[i]=0;
    		}
    	}
//    	下标 i 之后 的 k 个元素是 非递减的 。
    	for(int i=n-2;i>=0;i--) {
    		if(nums[i]<=nums[i+1]) {
    			increase[i]=increase[i+1]+1;
    		}else {
    			increase[i]=0;
    		}
    	}
    	List<Integer> res = new ArrayList<Integer>();
    	for(int i=k;i<n-k;i++) {
    		if(increase[i+1]+1>=k && decrease[i-1]+1>=k) {
    			res.add(i);
//    			System.out.println(res.get(res.size()-1));
    		}
    	}
    	return res;
    }
}
