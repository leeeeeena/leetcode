package contest;

public class contest312_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest312_2 obj = new contest312_2();
		int[] nums = {1,2,3,3,2,2};
		int res = obj.longestSubarray(nums);
		System.out.println(res);
	}
    public int longestSubarray(int[] nums) {
    	
    	
    	int n = nums.length;
    	int max = nums[0];
    	int count =1;
    	int curCnt =1;
    	for(int i=1;i<n;i++) {
    		if(nums[i]>max) {
    			max = nums[i];
    			curCnt = 1;
    			count = curCnt;
    		}else if(nums[i]==max && nums[i]==nums[i-1]) {
    			curCnt++;
    			count = Math.max(curCnt, count);
    		}else if(nums[i]==max && nums[i]!=nums[i-1]) {
    			curCnt = 1;
    		}
    	}
    	
    	return count;
    }
}
