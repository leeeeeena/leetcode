package contest;

public class contest327_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest327_3 obj = new contest327_3();
//		String word2 = "abcc", word1 = "aab";
		String word1 = "aab",word2 = "bccd";
		System.out.println(obj.isItPossible(word1, word2));
	}
    public boolean isItPossible(String word1, String word2) {
    	int[] chars1 = new int[26];
    	int[] chars2 = new int[26];
    	int len1 = word1.length();
    	int len2 = word2.length();
    	
    	int sum1 = 0;
    	for(int i=0;i<len1;i++) {
    		if(chars1[word1.charAt(i)-'a']==0) {
    			sum1++;
    		}
    		chars1[word1.charAt(i)-'a']++;
    	}
    	int sum2 = 0;
    	for(int i=0;i<len2;i++) {
    		if(chars2[word2.charAt(i)-'a']==0) {
    			sum2++;
    		}
    		chars2[word2.charAt(i)-'a']++;
    	}
    	if(sum1>sum2) {
    		int gap = sum1-sum2;
    		if(gap==1) {
    			//1\
    			//word1换到word2没有的且>=2的字符；
    			//word2换到word1有的，且>=2的字符；
    			boolean f1 = false;
    			boolean f2 = false;
    			for(int i=0;i<26;i++) {
    				if(chars1[i]>=2 && chars2[i]==0) {
    					f1 = true;
    					break;
    				}
    			}
    			for(int i=0;i<26;i++) {
    				if(chars1[i]>0 && chars2[i]>=2) {
    					f2 = true;
    					break;
    				}
    			}
    			if(f1&&f2) {
    				return true;
    			}
    			//2\
    			//word1换到word2没有的且==1的字符；
    			//word2换到word1有的，且==1的字符；
    			f1 = false;
    			f2 = false;
    			for(int i=0;i<26;i++) {
    				if(chars1[i]==1 && chars2[i]==0) {
    					f1 = true;
    					break;
    				}
    			}
    			for(int i=0;i<26;i++) {
    				if(chars2[i]==1 && chars1[i]>0) {
    					f2 = true;
    					break;
    				}
    			}
    			if(f1&&f2) {
    				return true;
    			}
    			return false;
//    			boolean f = false;
//    			for(int i=0;i<26;i++) {
//    				if(chars1[i]>=2 && chars2[i]==0) {
//    					f = true;
//    					break;
//    				}
//    			}
//    			if(f==false) {
//    				return false;
//    			}
//    			f=false;
//    			for(int i=0;i<26;i++) {
//    				if(chars1[i]>0 && chars2[i]>=2) {
//    					f = true;
//    					break;
//    				}
//    			}
//    			return f;
    		}else if(gap==2) {
    			//word1换到word2没有的且==1的字符；
    			//word2换到word1有的，且>=2的字符；
    			boolean f = false;
    			for(int i=0;i<26;i++) {
    				if(chars1[i]==1 && chars2[i]==0) {
    					f = true;
    					break;
    				}
    			}
    			if(f==false) {
    				return false;
    			}
    			f=false;
    			for(int i=0;i<26;i++) {
    				if(chars1[i]>0 && chars2[i]>=2) {
    					f = true;
    					break;
    				}
    			}
    			return f;
    		}else {
    			return false;
    		}
    		
    	}else if(sum1==sum2) {
    		//1\
			//word1换到word2有的, 且==2的字符；
			//word2换到word1有的，且==2的字符；
    		boolean f1 = false;
    		boolean f2 = false;
    		for(int i=0;i<26;i++) {
    			if(chars1[i]>=2 && chars2[i]>=0) {
					f1 = true;
					break;
				}
    		}
    		for(int i=0;i<26;i++) {
    			if(chars2[i]>=2 && chars1[i]>=0) {
					f2 = true;
					break;
				}
    		}
    		if(f1&&f2) {
    			return true;
    		}
    		//2\
    		//word1换到word2没有的, 且==1的字符；
    		//word2换到word1没有的, 且==1的字符；
    		f1 = false;
    		f2 = false;
    		for(int i=0;i<26;i++) {
    			if(chars1[i]==1 && chars2[i]==0) {
					f1 = true;
					break;
				}
    		}
    		for(int i=0;i<26;i++) {
    			if(chars2[i]==1 && chars1[i]==0) {
					f2 = true;
					break;
				}
    		}
    		if(f1&&f2) {
    			return true;
    		}
    		return false;
    	}else {
    		int gap = sum2-sum1;
    		if(gap==1) {
    			//1\
    			//word2换到word1没有的且>=2的字符；
    			//word1换到word2有的，且>=2的字符；
    			boolean f1 = false;
    			boolean f2 = false;
    			for(int i=0;i<26;i++) {
    				if(chars2[i]>=2 && chars1[i]==0) {
    					f1 = true;
    					break;
    				}
    			}
    			for(int i=0;i<26;i++) {
    				if(chars2[i]>0 && chars1[i]>=2) {
    					f2 = true;
    					break;
    				}
    			}
    			if(f1&&f2) {
    				return true;
    			}
    			//2\
    			//word2换到word1没有的且==1的字符；
    			//word1换到word2有的，且==1的字符；
    			f1 = false;
    			f2 = false;
    			for(int i=0;i<26;i++) {
    				if(chars2[i]==1 && chars1[i]==0) {
    					f1 = true;
    					break;
    				}
    			}
    			for(int i=0;i<26;i++) {
    				if(chars1[i]==1 && chars2[i]>0) {
    					f2 = true;
    					break;
    				}
    			}
    			if(f1&&f2) {
    				return true;
    			}
    			return false;
    		}else if(gap==2) {
    			//word1换到word2没有的且==1的字符；
    			//word2换到word1有的，且>=2的字符；
    			boolean f = false;
    			for(int i=0;i<26;i++) {
    				if(chars2[i]==1 && chars1[i]==0) {
    					f = true;
    					break;
    				}
    			}
    			if(f==false) {
    				return false;
    			}
    			f=false;
    			for(int i=0;i<26;i++) {
    				if(chars2[i]>0 && chars1[i]>=2) {
    					f = true;
    					break;
    				}
    			}
    			return f;
    		}else {
    			return false;
    		}
    	}
    	
    }

}
