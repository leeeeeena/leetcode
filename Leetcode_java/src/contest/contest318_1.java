package contest;

public class contest318_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest318_1 obj = new contest318_1();
		int[] nums = {1,2,2,1,1,0};
		int[] res = obj.applyOperations(nums);
		for(int i = 0;i<res.length;i++) {
			System.out.println(res[i]);
		}
	}
    public int[] applyOperations(int[] nums) {
    	int n = nums.length;
    	int[] res = new int[n];
    	for(int i=0;i<n-1;i++) {
    		if(nums[i]==nums[i+1]) {
    			nums[i]=2*nums[i];
    			nums[i+1]=0;
    		}
    	}
    	int idx=0;
    	for(int i=0;i<n;i++) {
    		if(nums[i]!=0) {
    			res[idx++]=nums[i];
    		}
    	}
    	return res;
    }
}
