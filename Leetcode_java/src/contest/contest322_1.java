package contest;

public class contest322_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest322_1 obj = new contest322_1();
		String sentence = "leetcode exercises sound delightful";
		System.out.println(obj.isCircularSentence(sentence));
	}
	public boolean isCircularSentence(String sentence) {
        String[] words = sentence.split(" ");
        for(int i=0;i<words.length;i++){
            if(i==0){
                if(words[words.length-1].charAt(words[words.length-1].length()-1)!=words[0].charAt(0)){
                    return false;
                }
            }else{
                if(words[i].charAt(0)!=words[i-1].charAt(words[i-1].length()-1)){
                    return false;
                }
            }
        }
        return true;
    }
}
