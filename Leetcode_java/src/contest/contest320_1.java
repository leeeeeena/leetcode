package contest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class contest320_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest320_1 obj = new contest320_1();
		int[] nums = {4,4,2,4,3};
		System.out.println(obj.unequalTriplets(nums));
	}
    public int unequalTriplets(int[] nums) {
    	Map<Integer,Integer> maps = new HashMap<Integer,Integer>();
    	
    	int n = nums.length;
    	for(int i=0;i<n;i++) {
    		if(!maps.containsKey(nums[i])) {
    			maps.put(nums[i], 1);
    		}else {
    			maps.put(nums[i], maps.get(nums[i])+1);
    		}
    	}
    	int m = maps.size();
    	if(m<3) {
    		return 0;
    	}
    	int[] distinctNum = new int[m];
    	int idx = 0;
    	for(Map.Entry<Integer,Integer> entry:maps.entrySet()){
    		distinctNum[idx++]=entry.getKey();
    	}
    	int res = 0;
    	for(int i=0;i<m;i++) {
    		for(int j=i+1;j<m;j++) {
    			for(int k=j+1;k<m;k++) {
    				res+=maps.get(distinctNum[i])*maps.get(distinctNum[j])*maps.get(distinctNum[k]);
    			}
    		}
    	}
    	return res;
    }
}
