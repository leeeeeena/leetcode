package contest;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Stack;

public class contest327_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest327_2 obj = new contest327_2();
		int[] nums = {1,10,3,3,3};
		int k = 3;
		System.out.println(obj.maxKelements(nums, k));
	}
    public long maxKelements(int[] nums, int k) {
    	long res = 0 ;
    	PriorityQueue<Integer>bigHeap=new PriorityQueue<>(new Comparator<Integer>() {
    	    @Override
    	    public int compare(Integer o1, Integer o2) {
    	        return o2-o1;
    	    }
    	});


    	int n = nums.length;
    	for(int i=0;i<n;i++) {
    		bigHeap.add(nums[i]);
    	}
    	
    	for(int i=0;i<k;i++) {
    		int tmp = bigHeap.poll();
    		res+=(long)tmp;
    		tmp = (int) Math.ceil((long)(tmp)/3.0);
    		bigHeap.add(tmp);
    	}
    	return res;
    }
}
