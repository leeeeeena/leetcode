package contest;

public class contest309_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest309_3 obj = new contest309_3();
		
		int[] nums = {1,3,8,48,10};
		int res = obj.longestNiceSubarray(nums);
		System.out.println(res);
	}
    public int longestNiceSubarray(int[] nums) {
        int ans = 0;
        for (int i = 0; i < nums.length; ++i) {
            int or = 0, j = i;
            while (j >= 0 && (or & nums[j]) == 0)
                or |= nums[j--];
            ans = Math.max(ans, i - j);
        }
        return ans;
    }
//	public int longestNiceSubarray(int[] nums) {
//        int n = nums.length;
//        int max = 1;
//        int[][] counts = new int[n][10];
//        int right = 0;
//        for(int i=0;i<n;i++){
//        	for(int j=0;j<10;j++) {
//        		int cur = nums[i]>>j &1;
//        		counts[i][j] = cur;
//        		System.out.print(cur+" ");
//        		if(cur==1) {
//        			right = j;
//        		}
//        	}
////        	System.out.println();
//            
//        }
////        System.out.println(right);
//        
//        for(int i=0;i<=right;i++){
//            int count = 0;
//            for(int j=0;j<n;j++){
////                int f = counts[i][j];
////                System.out.print(counts[j][i]+' ');
//                if(counts[j][i]==1){
//                    count++;
//                }
//            }
////            System.out.println();
//            max = Math.max(max,count);
//        }
//        return n-max;
//    }
}
