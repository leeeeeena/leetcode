package contest;

import java.util.Arrays;

public class contest322_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest322_2 obj = new contest322_2();
		int[] skill = {3,2,5,1,3,4};
		System.out.println(obj.dividePlayers(skill));
	}
	public long dividePlayers(int[] skill) {
        Arrays.sort(skill);
        int n = skill.length;
        if(n==2){
            return ((long)skill[0])*((long)skill[1]);
        }
        long sum = skill[0]+skill[n-1];
        long res = 0;
        for(int i =0;i<n/2;i++){
            if(skill[i]+skill[n-1-i]!=sum){
                return -1;
            }else{
                res += 1.0*skill[i]*skill[n-1-i];
            }
        }
        return res;
    }
}
