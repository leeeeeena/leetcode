package contest;

import java.util.HashMap;
import java.util.Map;

public class contest300_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest300_1 obj = new contest300_1();
		
		String key = "the quick brown fox jumps over the lazy dog";
		String message = "vkbs bs t suepuv";
		String res = obj.decodeMessage(key, message);
		System.out.println(res);
	}
	public String decodeMessage(String key, String message) {
        Map<Character,Character> maps = new HashMap<Character,Character>();
        int index = 0;
        int len = key.length();
        for(int i=0;i<len;i++){
            char c = key.charAt(i);
            if(!maps.containsKey(c) && c!=' '){
                maps.put(c,(char)('a'+index));
                index++;
            }
        }
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<message.length();i++){
            char c = message.charAt(i);
            System.out.println(c);
            if(!maps.containsKey(c)){
                sb.append(c);
            }else{
                sb.append(maps.get(c));
            }
        }
        return new String(sb);
    }
}
