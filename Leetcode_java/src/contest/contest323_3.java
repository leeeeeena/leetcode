package contest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class contest323_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest323_3 obj = new contest323_3();
		 Allocator all_obj = obj.new Allocator(10);
		 int param_1 = all_obj.allocate(1,1);
		 int param_2 = all_obj.free(1);
		 System.out.println(param_1);
		 System.out.println(param_2);
	}
	public class Allocator{
		Map<Integer,List<Integer>> mID_maps;
	    int[] allocators;
	    int a_size;
	    
	    public Allocator(int n) {
	        mID_maps = new HashMap<Integer,List<Integer>>();
	        allocators = new int[n];
	        a_size = n;
	    }
	    
	    public int allocate(int size, int mID) {
	        int left = -1 ;
	        int right = -1;
	        for(int i=0;i<a_size;i++){
	            if(allocators[i]!=0){
	                left = i;
	            }else{
	                if(i-left==size){
	                    right = i;
	                    break;
	                }
	            }
	        }
	        if(right-left<size){
	            return -1;
	        }
	        List<Integer> list;
	        if(!mID_maps.containsKey(mID)){
	            list = new ArrayList<Integer>();
	        }else{
	            list = mID_maps.get(mID);
	        }
	        for(int i=left+1;i<=right;i++){
	            allocators[i]=mID;
	            list.add(i);
	            mID_maps.put(mID,list);
	        }
	            
	        return left+1;
	        
	    }
	    
	    public int free(int mID) {
	        if(!mID_maps.containsKey(mID)){
	            return 0;
	        }
	        List<Integer> list = mID_maps.get(mID);
	        mID_maps.remove(mID);
	        
	        for(int i=0;i<list.size();i++){
	            int idx = list.get(i);
	            allocators[idx]=0;
	        }
	        return list.size();
	    }
	}
}

