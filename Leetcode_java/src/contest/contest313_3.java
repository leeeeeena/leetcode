package contest;

public class contest313_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest313_3 obj = new contest313_3();
		int num1 = 25;
		int num2 = 72;
		int res = obj.minimizeXor(num1, num2);
		System.out.println(res);
	}
	public int minimizeXor(int num1, int num2) {
        int bits = 30;
        int[] bit1 = new int[30];
        int[] bit2 = new int[30];
        
        int temp = num1;
        int temp_idx = 0;
        int max_bit1 = 0;
        while(temp>0){
        	if(temp%2==1) {
        		max_bit1=temp_idx;
        	}
            bit1[temp_idx++]=temp%2;
            temp=temp/2;
        }
        
        int cnt = 0;
        temp = num2;
        temp_idx = 0;
        while(temp>0){
            if(temp%2==1){
                cnt++;
            }
            bit2[temp_idx++]=temp%2;
            temp=temp/2;
        }
        
        int[] res = new int[30];
        int idx = max_bit1;
        while(idx>=0 && cnt>0){
            if(bit1[idx]==1){
                res[idx]=1;
                cnt--;
            }
            idx--;
        }
        idx = 0;
        while(cnt>0 && idx<30){
            if(res[idx]==0){
                res[idx]=1;
                cnt--;
            }
            idx++;
        }
        int finalres = 0;
        for(int i=0;i<30;i++){
            finalres+=res[i]*(Math.pow(2,i));
        }
        return finalres;
    }
}
