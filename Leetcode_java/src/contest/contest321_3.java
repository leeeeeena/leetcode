package contest;

import java.util.Stack;


public class contest321_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest321_3 obj = new contest321_3();
		//……
	}
	public class ListNode {
	      int val;
	      ListNode next;
	      ListNode() {}
	      ListNode(int val) { this.val = val; }
	      ListNode(int val, ListNode next) {
	          this.val = val;
	          this.next = next;
	      }
}
	public ListNode removeNodes(ListNode head) {
	        //stack
	        Stack<Integer> stack = new Stack<Integer>();
	        while(head!=null){
	            while(!stack.isEmpty() && stack.peek()<head.val){
	                stack.pop();
	            }
	            stack.push(head.val);
	            head=head.next;
	        }
	        //遍历完毕之后将所有的node推出，
	        int n = stack.size();
	        Integer[] nodes = new Integer[n];
	        for(int i=n-1;i>=0;i--){
	            nodes[i]=stack.pop();
	        }
	        //reverse
	        ListNode tmp = new ListNode(0);
	        ListNode pre = tmp;
	        for(int i=0;i<n;i++){
	            pre.next = new ListNode(nodes[i]);
	            pre = pre.next;
	        }
	        return tmp.next;
	    }
}
