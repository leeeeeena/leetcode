package contest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class contest323_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest323_2 obj = new contest323_2();
		int[] nums = {4,3,6,16,8,2};
		System.out.println(obj.longestSquareStreak(nums));
	}
	public int longestSquareStreak(int[] nums) {
        Map<Integer,Integer> maps = new HashMap<Integer,Integer>();
        int n = nums.length;
        
        Arrays.sort(nums);
        int max = -1;
        for(int i=n-1;i>=0;i--){
            int square = nums[i]*nums[i];
            if(!maps.containsKey(square)){
                maps.put(nums[i],1);
            }else{
                maps.put(nums[i],maps.get(square)+1);
                max = Math.max(max,maps.get(square)+1);
            }
            
            
        }
        return max;
        
    }
}
