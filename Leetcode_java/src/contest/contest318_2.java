package contest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class contest318_2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest318_2 obj = new contest318_2();
//		int[] nums = {1,5,4,2,9,9,9};
//		int k = 3;
//		int[] nums = {1,1,1,7,8,9};
//		int k = 3;
//		int[] nums = {11,11,7,2,9,4,17,1};
//		int k = 1;
		int[] nums = {9,18,10,13,17,9,19,2,1,18};
		int k = 5;
		long res = obj.maximumSubarraySum(nums,k);
		System.out.println(res);
	}
    public long maximumSubarraySum(int[] nums, int k) {
    	long max_res = 0;
    	int n = nums.length;
    	int left = 0;
    	long sum = nums[0];
    	Map<Integer,Integer> map = new HashMap<Integer,Integer>();
    	map.put(nums[0], 0);
    	if(k==1) {
    		for(int i=0;i<n;i++) {
    			max_res = Math.max(max_res, nums[i]);
    		}
    		return max_res;
    	}
    	for(int i=1;i<n;i++) {
    		if(i-left+1<=k) {
    			if(map.containsKey(nums[i])) {
    				int before = map.get(nums[i]);
    				left = before+1;
    				List<Integer> remove_list = new ArrayList<Integer>();
    				for(Map.Entry<Integer,Integer> m:map.entrySet()) {
    					if(m.getValue()<before) {
    						sum-=m.getKey();
    						remove_list.add(m.getKey());
    					}
    				}
    				for(int x=0;x<remove_list.size();x++) {
						map.remove(remove_list.get(x));
    				}
    				map.put(nums[i],i);
    			}else {
    				sum+=nums[i];
    				map.put(nums[i],i);
    				if(i+1-left==k) {
    					max_res = Math.max(max_res, sum);
    				}
    			}
    		}else {
    			sum-=nums[left];
    			map.remove(nums[left]);
    			if(map.containsKey(nums[i])) {
    				int before = map.get(nums[i]);
    				left = before+1;
    				List<Integer> remove_list = new ArrayList<Integer>();
    				for(Map.Entry<Integer,Integer> m:map.entrySet()) {
    					if(m.getValue()<before) {
    						sum-=m.getKey();
    						remove_list.add(m.getKey());
    					}
    				}
    				for(int x=0;x<remove_list.size();x++) {
						map.remove(remove_list.get(x));
    				}
    				map.put(nums[i],i);
    				if(i-left+1==k) {
    					max_res = Math.max(max_res, sum);
    				}
    			}else {
    				left++;
    				sum+=nums[i];
    				map.put(nums[i], i);
    				max_res = Math.max(max_res, sum);
    			}
    		}
    		
    	}
    	return max_res;
    }
}
