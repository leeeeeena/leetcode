package contest;

public class contest322_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest322_3 obj = new contest322_3();
//		int n= 7;
//		int[][] roads = {{1,3,1484},{3,2,3876},{2,4,6823},{6,7,579},{5,6,4436},{4,5,8830}};
		int n = 14;
		int[][] roads = {{12,7,2151},{7,2,7116},{11,14,8450},{11,2,9954},
				{1,11,3307},{10,7,3561},{10,1,4986},{11,7,7674},{14,2,1764},
				{11,12,6608},{14,7,1070},{9,8,2287},{14,12,6559},{1,2,1450},
				{2,12,9165}};
		int score = obj.minScore(n, roads);
		System.out.println(score);
	}
//	public int min = 10001;
    public int minScore(int n, int[][] roads) {
        int[][] father  = new int[n+1][2];
        for(int i=0;i<=n;i++){
            father[i][0] = i;
             father[i][1] = 10001;
        }
        for(int i=0;i<roads.length;i++){
            int node1 = roads[i][0];
            int node2 = roads[i][1];
            int val = roads[i][2];
            father[node1][1] = Math.min(father[getFather(father,node1)][1], val);
            father[node2][1] = Math.min(father[getFather(father,node2)][1], val);
            if(getFather(father,node1)!=getFather(father,node2)){
                join(father,node1,node2);
            }
//            else{
////                if(getFather(father,node1)==1){
//            	father[getFather(father,node1)][1] = Math.min(Math.min(father[node1][1],father[node2][1]),Math.min(val,father[getFather(father,node1)][1]));
////            	father[getFather(father,node2)][1] = father[node1][1];
////                }
//            }
            int f = getFather(father,node1);
            father[f][1] = Math.min(father[node1][1], Math.min(father[f][1],father[node2][1]));
            
        }
        return father[1][1];
    }
    
    public Integer getFather(int[][] father, int u) {
        if (father[u][0] != u) {
            father[u][0] = getFather(father, father[u][0]);
        }
        return father[u][0];
    }
 
    public void join(int[][] father,int x, int y) {
        int fx = getFather(father,x);
        int fy = getFather(father,y);
        if (fx != fy){
            if(fx>fy){
                father[fx][0] = fy;
            }else{
                father[fy][0] = fx;
            }
        }
        
//         father[fx][1] = Math.min(Math.min(father[x][1],father[y][1]),Math.min(father[fx][1],val));
//         father[fy][1] = father[fx][1];
    }
}
