package contest;

import java.util.List;

public class contest284_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		int[] nums = {5,2,2,4,0,6};
//		int k = 4;
		int[] nums = {35,43,23,86,23,45,84,2,18,83,79,28,54,81,
						12,94,14,0,0,29,94,12,13,1,48,85,22,95,
						24,5,73,10,96,97,72,41,52,1,91,3,20,22,
						41,98,70,20,52,48,91,84,16,30,27,35,69,
						33,67,18,4,53,86,78,26,83,13,96,29,15,34,80,16,49};
		int k = 15;
		
//		int[] nums = {2,5,6,2,10,18,14,7,3,0,18,15,10,9,11,19,
//				3,19,4,8,10,14,11,5,2,17,0,2,0,11,12,11,2,9,2,
//				6,0,14,7,19,17,13,4,4,11,19,9,1,10};
//		
//		int k = 6;
		
		
		contest284_3 obj = new contest284_3();
		int res = obj.maximumTop(nums, k);
		System.out.println(res);
	}
    public int maximumTop(int[] nums, int k) {
        int max = 0, n = nums.length;
        if(n == k % 2) return -1;
        
        for(int i = 0; i < Math.min(k - 1, n); i++){
            max = Math.max(max, nums[i]);
        }
        
        return Math.max(max, k < n ? nums[k] : 0);
    }

}
