package contest;

public class contest306_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest306_3 obj = new contest306_3();
		String pattern = "IIIDIDDD";
//		String pattern = "DDDIII";
//		String pattern =  "DDD";
		String s = obj.smallestNumber(pattern);
		System.out.println(s);
	}

	public String smallestNumber(String pattern) {
		int i = 0, n = pattern.length();
		var cur = '1';
		var ans = new char[n + 1];
		while (i < n) {
			if (i > 0 && pattern.charAt(i) == 'I')
				++i;
			for (; i < n && pattern.charAt(i) == 'I'; ++i)
				ans[i] = cur++;
			var i0 = i;
			while (i < n && pattern.charAt(i) == 'D')
				++i;
			for (var j = i; j >= i0; --j)
				ans[j] = cur++;
		}
		return new String(ans);
	}

//
//	作者：endlesscheng
//	链接：https://leetcode.cn/problems/construct-smallest-number-from-di-string/solution/by-endlesscheng-8ee3/
//	来源：力扣（LeetCode）
//	著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
//	public String smallestNumber(String pattern) {
//        int m = pattern.length();
//        int n = m+1;
//        char[] nums = new char[n];
//        int cur_max_val = n;
//        
//        //从右往左边找峰顶
//        int r = n;
//        for(int i=m-2;i>=0;i--){
//            if(pattern.charAt(i)=='I' && pattern.charAt(i+1)=='D'){
//                for(int j=i+1;j<r;j++){
//                    nums[j]=(char)('0'+cur_max_val);
//                    cur_max_val--;
//                }
//                r = i;
//            }
//        }
//        //test
////        System.out.print("_ ");
////         for(int k=0;k<n;k++){
////             System.out.print(nums[k]+" _");
////         }
//         System.out.println("_");
//        //从左往右找峰谷
//        int l=-1;
//        int cur_min_val = 1;
//        for(int i=1;i<m;i++) {
//        	if(pattern.charAt(i)=='I' && pattern.charAt(i-1)=='D') {
//        		for(int j=i;j>l;j--) {
//        			if(nums[j]!=0) {
//        				continue;
//        			}
//        			nums[j]=(char)('0'+cur_min_val);
//        			cur_min_val+=1;
//        		}
//        		l=i;
//        	}
//        }
//        //test
////        System.out.print("_");
////        for(int k=0;k<n;k++){
////            System.out.print(nums[k]+" _");
////        }
////        System.out.println("_");
//        
//        for(int i=l+1;i<r;i++) {
//        	if(i<m && pattern.charAt(i)=='I') {
//	        	nums[i]=(char)('0'+cur_min_val);
//	        	cur_min_val++;
//        	}else if (i<m && pattern.charAt(i)=='D') {
//	        	nums[i]=(char)('0'+cur_max_val);
//	        	cur_max_val--;
//        	}
//        }
//        if(nums[n-1]==0) {
//        	nums[n-1]=(char)('0'+cur_min_val);
//        }
//
//        return String.valueOf(nums);
//    }
}
