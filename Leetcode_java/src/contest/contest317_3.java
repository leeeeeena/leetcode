package contest;

public class contest317_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest317_3 obj = new contest317_3();
		long n = 16;
		int target = 6;
		long cnt = obj.makeIntegerBeautiful(n, target);
		System.out.println(cnt);
	}
    public long makeIntegerBeautiful(long n, int target) {
    	long t = 1;
    	long res = 0;
    	while(true) {
    		long next_n = n + (t-n%t)%t;
    		long temp = next_n;
    		int sum = 0;
    		while(temp>0) {
    			sum+=temp%10;
    			temp=temp/10;
    		}
    		if(sum<=target) {
    			return next_n-n;
    		}
    		t=t*10;
    	}
//    	return res;
    	
    }
}
