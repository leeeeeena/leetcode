package contest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class contest305_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest305_2 obj = new contest305_2();
		int n = 7;
		int[][] edges = {{0,1},{1,2},{3,1},{4,0},{0,5},{5,6}};
		int[] restricted = {4,5};
		int res = obj.reachableNodes(n, edges, restricted);
		System.out.println(res);
	}
	public int reachableNodes(int n, int[][] edges, int[] restricted) {
        boolean[] visited = new boolean[n];
        // System.out.println(visited[0]);
        Map<Integer,List<Integer>> edgeMap = new HashMap<Integer,List<Integer>>();
        for(int i=0;i<n;i++){
            edgeMap.put(i,new ArrayList<Integer>());
        }
        for(int i=0;i<edges.length;i++){
            int e1 = edges[i][0];
            int e2 = edges[i][1];
            edgeMap.get(e1).add(e2);
            edgeMap.get(e2).add(e1);
        }
        for(int i=0;i<restricted.length;i++){
            visited[restricted[i]]=true;
        }
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(0);
         visited[0]=true;
        int count = 0;
        while(!queue.isEmpty()){
            int size = queue.size();
            for(int i=0;i<size;i++){
                count++;
                int next = queue.poll();
                
                List<Integer> nextList = edgeMap.get(next);
                for(int j=0;j<nextList.size();j++){
                    int temp = nextList.get(j);
                    if(visited[temp]==false){
                        queue.add(temp);
                        visited[next]=true;
                    } 
                }
                
            }
            
        }
        return count;
    }
}
