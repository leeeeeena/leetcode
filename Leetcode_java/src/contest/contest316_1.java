package contest;

public class contest316_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest316_1 obj = new contest316_1();
		String[] event1 = {"10:00","11:00"}, event2 = {"14:00","15:00"};
		boolean flag = obj.haveConflict(event1, event2);
		System.out.println(flag);
	}
    public boolean haveConflict(String[] event1, String[] event2) {
    	int startTime1 = Integer.valueOf(event1[0].split(":")[0])*60+Integer.valueOf(event1[0].split(":")[1]);
    	int endTime1 = Integer.valueOf(event1[1].split(":")[0])*60+Integer.valueOf(event1[1].split(":")[1]);
    	int startTime2 = Integer.valueOf(event2[0].split(":")[0])*60+Integer.valueOf(event2[0].split(":")[1]);
    	int endTime2 = Integer.valueOf(event2[1].split(":")[0])*60+Integer.valueOf(event2[1].split(":")[1]);
    	
    	if(startTime1>endTime2 || endTime1<startTime2 || startTime2>endTime1 || endTime2<startTime1) {
    		return false;
    	}else {
    		return true;
    	}
    }
}
