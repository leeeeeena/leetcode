package contest;

public class contest316_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest316_2 obj = new contest316_2();
		int[] nums = {9,3,1,2,6,3};
		int k = 3;
		int res = obj.subarrayGCD(nums, k);
		System.out.println(res);
	}
    public int subarrayGCD(int[] nums, int k) {
    	int n = nums.length;
    	int cnt = 0;
    	int[][] dp = new int[n][n];
    	for(int i=0;i<n;i++) {
    		for(int j=i;j<n;j++) {
    			if(i==j) {
    				dp[i][j]=nums[i];
    			}else {
    				dp[i][j] = gcd(dp[i][j-1],nums[j]);
    			}
    			
    			if(dp[i][j]==k) {
    				cnt++;
    			}
    		}
    	}
    	return cnt;
    }
    
    public int gcd(int a,int b) {
		int gcd = 0,c;                       
		if(a > b) 
		{
			while(b != 0)
           {
				a = a % b;                   
				if(a < b)
				{
				c = a;
				a = b;
				b = c;
				}
				gcd = a;
			}
		}
		if(a == b)
			gcd = a;
		else 
		{
			c = a;a = b;b = c;
			while(b != 0)
	        {
				a = a % b;
				if(a < b){c = a;a = b;b = c;}
				gcd = a;
			}
		}
		return gcd;
	}

}
