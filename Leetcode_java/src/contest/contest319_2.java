package contest;

public class contest319_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
    public int subarrayLCM(int[] nums, int k) {
    	int cnt = 0;
    	int n = nums.length;
    	int[][] dp = new int[n][n];
    	for(int i=0;i<n;i++) {
            if(nums[i]>k || k%(nums[i])!=0){
                dp[i][i]=0;
            }
    		dp[i][i] = nums[i];
    		if(dp[i][i]==k) {
    			cnt++;
    		}
    	}
    	
    	for(int i=0;i<n;i++) {
    		for(int j=i+1;j<n;j++) {
                if(dp[i][j-1]==0){
                    break;
                }
                int cm = getCM(dp[i][j-1],dp[j][j]);
    			dp[i][j]=cm>k?0:cm;
    			if(dp[i][j]==k) {
    				cnt++;
    			}
    		}
    	}
    	return cnt;
    }
	//计算最小公倍数
    public static int getCM(int m, int n){
        if(m==0||n==0){
            return 0;
        }
        //计算m、n中较大者
        int max=Math.max(m,n);
        int min = Math.min(m,n);
        //从max到m*n之间找最小公倍数
        for(int i=1;i*max<=m*n;i++){
            //如果既能被m整除又能被n整除，说明是最小公倍数，直接返回
            int cur = max*i;
            if(cur%m==0&&cur%n==0){
                return cur;
            }
        }
        return 0;
    }

}
