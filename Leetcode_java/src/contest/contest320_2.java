package contest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.tree.TreeNode;

public class contest320_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public class TreeNode {
		      int val;
		      TreeNode left;
		      TreeNode right;
		      TreeNode() {}
		      TreeNode(int val) { this.val = val; }
		      TreeNode(int val, TreeNode left, TreeNode right) {
		          this.val = val;
		          this.left = left;
		          this.right = right;
		      }
	}
		 
    public List<List<Integer>> closestNodes(TreeNode root, List<Integer> queries) {
    	int[] nums = new int[1000000];
    	Queue<TreeNode> queue = new LinkedList<TreeNode>();
    	queue.offer(root);
    	int min = 1000000;
    	int max = 0;
    	while(!queue.isEmpty()) {
    		int size = queue.size();
    		for(int i=0;i<size;i++) {
    			TreeNode cur = queue.poll();
    			if(cur.left!=null) {
    				queue.offer(cur.left);
    			}
    			if(cur.right!=null) {
    				queue.offer(cur.right);
    			}
    			nums[cur.val] = cur.val;
                min = Math.min(cur.val,min);
                max = Math.max(cur.val,max);
    		}
    	}
    	int n = queries.size();

    	List<List<Integer>> res = new ArrayList<List<Integer>>();
    	for(int i=0;i<n;i++) {
    		int now = queries.get(i);
    		int left = now;
    		int right = now;
    		while(left>=min && nums[left]==0) {
    			left--;
    		}
    		while(right<=max && nums[right]==0) {
    			right++;
    		}
    		left = (left<=min-1? -1:left);
    		right = (right>=max+1?-1:right);
    		List<Integer> curList = new ArrayList<Integer>();
    		curList.add(left);
    		curList.add(right);
    		res.add(curList);
    	}
    	return res;
    }
}
