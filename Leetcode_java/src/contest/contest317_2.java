package contest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class contest317_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest317_2 obj = new contest317_2();
//		String[] creators = {"alice","bob","alice","chris"};
//		String[] ids = {"one","two","three","four"};
//		int[] views = {5,10,5,4};
		
//		String[] creators = {"alice","alice","alice"};
//		String[] ids = {"a","b","c"};
//		int[] views = {1,2,2};
		
		String[] creators = {"a","a"};
		String[] ids = {"aa","a"};
		int[] views = {5,5};
		
		List<List<String>> res = obj.mostPopularCreator(creators, ids, views);
		for(int i=0;i<res.size();i++) {
			for(int j=0;j<res.get(i).size();j++) {
				System.out.print(res.get(i).get(j)+" ");
			}
			System.out.println();
		}
	}

    public List<List<String>> mostPopularCreator(String[] creators, String[] ids, int[] views) {
    	Set<String> creators_set = new HashSet<String>();
    	int n = creators.length;
    	for(int i=0;i<n;i++) {
    		creators_set.add(creators[i]);
    	}
    	int len = creators_set.size();
    	Integer[] idx_arr = new Integer[len];
    	for(int i=0;i<len;i++) {
    		idx_arr[i]=i;
    	}
    	Integer[] creators_first_idx_arr = new Integer[len];
    	String[] actual_creators_arr = new String[len];
    	Double[] total_views_arr = new Double[len];
    	Integer[] actual_most_vew_idx_arr = new Integer[len];
    	
    	Map<String,Integer> creators_map = new HashMap<String,Integer>();
    	int left = 0;
    	Double max_view = 0.0;
    	for(int i=0;i<n;i++) {
    		if(!creators_map.containsKey(creators[i])) {
//    			creators_first_idx_arr[left]=i;
    			actual_creators_arr[left] = creators[i];
    			total_views_arr[left] = 0.0+views[i];
    			actual_most_vew_idx_arr[left] = i;
    			
    			if(max_view<total_views_arr[left]) {
    				max_view = total_views_arr[left];
    			}
    			creators_map.put(creators[i], left++);
    		}else {
    			int creator_idx = creators_map.get(creators[i]);
    			total_views_arr[creator_idx] += views[i];
    			if(views[actual_most_vew_idx_arr[creator_idx]]<views[i]) {
    				actual_most_vew_idx_arr[creator_idx]=i;
    			}else if(views[actual_most_vew_idx_arr[creator_idx]]==views[i]) {
    				if(ids[actual_most_vew_idx_arr[creator_idx]].compareTo(ids[i])>0) {
    					actual_most_vew_idx_arr[creator_idx]=i;
    				}
    			}
    			if(max_view<total_views_arr[creator_idx]) {
    				max_view = total_views_arr[creator_idx];
    			}
    		}
    	}
    	List<List<String>> res = new ArrayList<List<String>>();
    	for(int i=0;i<len;i++) {
    		if(total_views_arr[i].equals(max_view)) {
    			List<String> cur = new ArrayList<String>();
    			cur.add(actual_creators_arr[i]);
    			cur.add(ids[actual_most_vew_idx_arr[i]]);
    			res.add(cur);
    		}
    	}
    	return res;    	
    }
}

