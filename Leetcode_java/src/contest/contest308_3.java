package contest;

import java.util.Arrays;

public class contest308_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest308_3 obj = new contest308_3();
		String[] garbage = {"G","P","GP","GG"};
		int[] travel = {2,4,3};
		int res = obj.garbageCollection(garbage, travel);
		System.out.println(res);
	}
    public int garbageCollection(String[] garbage, int[] travel) {
        int n = garbage.length;
        int[][] count = new int[n][3];
        
        for(int i=0;i<n;i++){
            String cur = garbage[i];
            for(int j=0;j<cur.length();j++){
                if(cur.charAt(j)=='M'){
                    count[i][0]++;
                }else if(cur.charAt(j)=='P'){
                    count[i][1]++;
                }else if(cur.charAt(j)=='G'){
                    count[i][2]++;
                }
            }
        }

        
        int res = 0;
        int[] max_distance = new int[3];
        int cur_distance = 0;
        for(int i=0;i<n;i++){
            for(int j=0;j<3;j++){
                if(count[i][j]>0){
                    res+=count[i][j];
                    max_distance[j]=cur_distance;
                }
            }
            if(i==n-1){
                break;
            }
            cur_distance+=travel[i];
        }
        return res+max_distance[0]+max_distance[1]+max_distance[2];
    }
}
