package contest;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class contest319_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest319_3 obj = new contest319_3();
		
	}

	public int minimumOperations(TreeNode root) {
		Queue<TreeNode> queue = new LinkedList<TreeNode>();

		if (root == null) {
			return 0;
		}
		int res = 0;
		queue.offer(root);
		while (!queue.isEmpty()) {
			int curSize = queue.size();
			int[] curArr = new int[curSize];
			for (int i = 0; i < curSize; i++) {
				TreeNode t = queue.poll();
				curArr[i] = t.val;
				if (t.left != null) {
					queue.offer(t.left);
				}
				if (t.right != null) {
					queue.offer(t.right);
				}
			}
			res += InsertSort(curArr);
		}
		return res;
	}

	public static int InsertSort(int[] source) {
		int cnt = 0;
		int len = source.length;
		for (int i = 0; i < len; i++) {
			int min = i;
			for (int j = i + 1; j < len; j++) {
				if (source[j] < source[min]) {
					min = j;
				}
			}
			if (min == i) {
				continue;
			} else {
				int temp = source[i];
				source[i] = source[min];
				source[min] = temp;
				cnt++;
			}
		}
		System.out.println(cnt);
		return cnt;
	}

	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}
	}

}
