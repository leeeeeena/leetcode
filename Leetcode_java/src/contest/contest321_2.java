package contest;

public class contest321_2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest321_2 obj = new contest321_2();
		String s = "coaching", t = "coding";
		System.out.println(obj.appendCharacters(s,t));
	}
	
	public int appendCharacters(String s, String t) {
        int n = s.length();
        int m = t.length();
         int slow = 0;
         for(int i=0;i<n;i++){
             if(slow<m && s.charAt(i)==t.charAt(slow)){
                 slow++;
             }
         }
         return slow>=m?0:m-slow;
     }
}
