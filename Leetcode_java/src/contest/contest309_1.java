package contest;

import java.util.HashSet;
import java.util.Set;

public class contest309_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest309_1 obj = new contest309_1();
		String s = "abaccb";
		int[] distance = {1,3,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		boolean res = obj.checkDistances(s, distance);
		System.out.println(res);
	}
    public boolean checkDistances(String s, int[] distance) {
        char[] chars = s.toCharArray();
        int n = distance.length;
        int[] res = new int[n];
        Set<Character> set = new HashSet<Character>();
        
        for(int i=0;i<s.length();i++){
            char c = chars[i];
            if(set.contains(c)) {
            	res[c-'a']+=i;
            	if(distance[c-'a']-res[c-'a']!=-1) {
            		return false;
            	}
            }else {
            	res[c-'a']-=i;
            	set.add(c);
            }

        }

        return true;
    }
}
