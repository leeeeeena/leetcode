package contest;

import java.util.HashMap;
import java.util.Map;

public class contest328_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest328_3 obj = new contest328_3();
//		int[] nums = {1,1,1,1,1};
//		int k = 10;
//		int[] nums = {3,1,4,3,2,2,4};
//		int k = 2;
		int[] nums = {2,3,1,3,2,3,3,3,1,1,3,2,2,2};
		int k=18;
		System.out.println(obj.countGood(nums, k));
	}
    public long countGood(int[] nums, int k) {
        int n = nums.length;
        int left = -1;
        int right = 0;
        long ans = 0;
        Map<Integer,Integer> maps = new HashMap<Integer,Integer> (); //key是数字，value是这个数字在当前子数组中出现的次数
        int times = 0;
        while(left<n && left<right){
            while(left>=0 && left<n && left<right) {
                int cur = nums[left];
                if(maps.get(cur)>=2){
                    times-=(maps.get(cur)-1);
                	if(times>=k) {
                		ans+=(long)(n-right+1);
                	}
                    maps.put(cur,maps.get(cur)-1);
//                    break;
                }else{
                	if(times>=k) {
                		ans+=(long)(n-right+1);
                	}
                    maps.put(cur,maps.get(cur)-1);
                }
                if(times<k) {
                	break;
                }
                left++;
            }
            while(right<n){
                int tmp = nums[right];
                if(maps.containsKey(tmp)){
                    times+=maps.get(tmp);
                    maps.put(tmp,maps.get(tmp)+1);
                }else{
                    maps.put(tmp,1);
                }
                right++;
                if(times>=k){
                    ans+=(long)(n-right+1);
                    break;
                }
                
            }
            if(right>=n && times<k) {
            	break;
            }
            left++;
        }
        return ans;
    }
}
