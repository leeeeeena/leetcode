package contest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class contest284_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = {3,4,9,1,3,9,5};
		int key = 9, k = 1;
		
		contest284_1 obj = new contest284_1();
		List<Integer> res = obj.findKDistantIndices(nums, key, k);
		
		for(int i=0;i<res.size();i++) {
			System.out.println(res.get(i));
		}
	}
    public List<Integer> findKDistantIndices(int[] nums, int key, int k) {
    	List<Integer> res = new ArrayList<Integer>();
    	
    	int n = nums.length;
    	
    	List<Integer> indexes = new ArrayList<Integer>();
    	for(int i=0;i<n;i++) {
    		if(nums[i]==key) {
    			indexes.add(i);
    		}
    	}
    	int last = 0;
    	for(int i=0;i<indexes.size();i++) {
    		int curIdx = indexes.get(i);
    		int left = curIdx-k>=last?curIdx-k:last;
    		int right = curIdx+k<n?curIdx+k:n-1;
    		for(int j=left;j<=right;j++) {
    			if(res.contains(j)) {
    				continue;
    			}else {
    				res.add(j);
    			}
    		}
    		last = right;
    		
    	}
    	Collections.sort(res);
    	return res;
    	
    }
}
