package contest;

public class contest321_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest321_1 obj = new contest321_1();
		System.out.println(obj.pivotInteger(8));
	}
    public int pivotInteger(int n) {
        int sum = (n+1)*n/2;
        for(int i=1;i<=n;i++){
            int left = (i+1)*i/2;
            int right = sum - (i-1)*i/2;
            if(left==right){
                return i;
            }
        }
        return -1;
    }
}
