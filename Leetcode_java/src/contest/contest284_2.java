package contest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class contest284_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 2;
		int[][] artifacts = {{0,0,0,0},{0,1,1,1}};
		int[][] dig = {{0,0},{0,1},{1,1}};
		
		
		contest284_2 obj = new contest284_2();
		int res = obj.digArtifacts(n, artifacts, dig);
		System.out.println(res);
	}
    public int digArtifacts(int n, int[][] artifacts, int[][] dig) {
    	int res = 0;
    	
    	Map<Integer,Set<Integer>> digPos = new HashMap<Integer,Set<Integer>>();
    	
    	for(int i=0;i<dig.length;i++) {
    		int curRow = dig[i][0];
    		int curCol = dig[i][1];
    		
    		Set<Integer> cols;
    		if(digPos.containsKey(curRow)) {
    			cols = digPos.get(curRow); 
    		}else {
    			cols = new HashSet<Integer>();
    		}
    		cols.add(curCol);
    		digPos.put(curRow,cols);
    	}
    	
    	for(int i=0;i<artifacts.length;i++) {
    		int topRow = artifacts[i][0];
    		int leftCol = artifacts[i][1];
    		int downRow = artifacts[i][2];
    		int rightCol = artifacts[i][3];
    		
    		int count = 0;
    		int total = 0;
    		for(int k= topRow;k<=downRow;k++) {
    			for(int l=leftCol;l<=rightCol;l++) {
//    				System.out.println(k+" "+l);
    				total++;
    				if(digPos.containsKey(k) && digPos.get(k).contains(l)) {
    					count++;
    				}
    			}
    		}
    		if(count==total) {
    			res++;
    		}
    		
    	}
    	
    	
    	return res;
    	
    }
}
