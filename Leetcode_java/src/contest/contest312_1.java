package contest;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class contest312_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest312_1 obj = new contest312_1();
		
		String[] names = {"Mary","John","Emma"};
		int[] heights = {180,165,170};
		
		String[] res = obj.sortPeople(names, heights);
		for(int i=0;i<res.length;i++) {
			System.out.println(res[i]);
		}
	}
    public String[] sortPeople(String[] names, int[] heights) {
    	int n = names.length;
    	Integer[] idx = new Integer[n];
    	for(int i=0;i<n;i++) {
    		idx[i]=i;
    	}
    	
//    	Arrays.sort(idx,(a,b)->(heights[a]-heights[b]));
    	Arrays.sort(idx,new Comparator<>() {

			@Override
			public int compare(Integer a, Integer b) {
				// TODO Auto-generated method stub
				return heights[b]-heights[a];
			}
    		
    	});
    	
    	String[] res = new String[n];
    	for(int i=0;i<n;i++) {
    		res[i]=names[idx[i]];
    	}
    	return res;
    }
}
