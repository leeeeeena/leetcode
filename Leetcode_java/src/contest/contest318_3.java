package contest;

import java.util.PriorityQueue;

public class contest318_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest318_3 obj = new contest318_3();
//		int[] costs = {17,12,10,2,7,2,11,20,8};
//		int k = 3;
//		int candidates = 4;
//		int[] costs = {31,25,72,79,74,65,84,91,18,59,27,9,81,33,17,58};
//		int k = 11;
//		int candidates = 2;
		int[] costs = {69,10,63,24,1,71,55,46,4,61,78,21,85,52,83,77,42,21,73,2,80,99,98,89,55,94,63,50,43,62,14};
		int k = 21;
		int candidates = 31;
		
		long res = obj.totalCost(costs, k, candidates);
		System.out.println(res);
		
	}
    public long totalCost(int[] costs, int k, int candidates) {

    	PriorityQueue<Integer> leftQueue = new PriorityQueue<>();
        PriorityQueue<Integer> rightQueue = new PriorityQueue<>();

        int n = costs.length, l = 0, r = n - 1;
        for (int i = 0; i < candidates; i++) {
            if (l <= r) {
                leftQueue.offer(costs[l++]);
            }
            if (l <= r) {
                rightQueue.offer(costs[r--]);
            }
        }

        long totalCost = 0;
        int cnt = 0;
        while (cnt++ < k) {
            int leftMin = leftQueue.isEmpty() ? Integer.MAX_VALUE : leftQueue.peek();
            int rightMin = rightQueue.isEmpty() ? Integer.MAX_VALUE : rightQueue.peek();

            if (leftMin <= rightMin) {
                leftQueue.poll();

                if (l <= r) {
                    leftQueue.offer(costs[l++]);
                }
            } else {
                rightQueue.poll();
                if (l <= r) {
                    rightQueue.offer(costs[r--]);
                }
            }

            totalCost += Math.min(leftMin, rightMin);
        }

        return totalCost;


    }
}
