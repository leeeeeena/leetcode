package contest;

import java.util.ArrayList;
import java.util.List;

public class contest304_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contest304_3 obj = new contest304_3();
		int[] edges = {2,2,3,-1};
		int node1 = 0;
		int node2 = 1;
		int res = obj.closestMeetingNode(edges, node1, node2);
		System.out.println(res);
	}
	public int closestMeetingNode(int[] edges, int node1, int node2) {
        List<Integer> list1 = new ArrayList<Integer>();
        List<Integer> list2 = new ArrayList<Integer>();
        
        int start = node1;
        while(start!=-1){
            list1.add(start);
            start = edges[start];
        }
        start = node2;
        while(start!=-1){
            list2.add(start);
            start = edges[start];
        }
        
        int size1 = list1.size();
        int size2 = list2.size();
        
        if(list1.get(size1-1)!=list2.get(size2-1)){
            return -1;
        }
        int idx1 = size1-1;
        int idx2 = size2-1;
        while(idx1>=0 && idx2>=0 && list1.get(idx1)==list2.get(idx2)){
            idx1--;
            idx2--;
        }
        return list1.get(idx1+1);
    }
}
