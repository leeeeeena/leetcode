package november22;

public class Leetcode1684 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1684 obj = new Leetcode1684();
		String allowed = "ab";
		String[] words = {"ad","bd","aaab","baa","badab"};
		int res = obj.countConsistentStrings(allowed, words);
		System.out.println(res);
	}
	public int countConsistentStrings(String allowed, String[] words) {
        int mask = 0;
        for (int i = 0; i < allowed.length(); i++) {
            char c = allowed.charAt(i);
            mask |= 1 << (c - 'a');
        }
        int res = 0;
        for (String word : words) {
            int mask1 = 0;
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                mask1 |= 1 << (c - 'a');
            }
            if ((mask1 | mask) == mask) {
                res++;
            }
        }
        return res;
    }
}
