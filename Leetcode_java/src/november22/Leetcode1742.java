package november22;

import java.util.HashMap;
import java.util.Map;

public class Leetcode1742 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1742 obj = new Leetcode1742();
		int lowLimit=1, highLimit=10;
		System.out.println(obj.countBalls(lowLimit, highLimit));
	}
    public int countBalls(int lowLimit, int highLimit) {
        Map<Integer, Integer> count = new HashMap<Integer, Integer>();
        int res = 0;
        for (int i = lowLimit; i <= highLimit; i++) {
            int box = 0, x = i;
            while (x != 0) {
                box += x % 10;
                x /= 10;
            }
            count.put(box, count.getOrDefault(box, 0) + 1);
            res = Math.max(res, count.get(box));
        }
        return res;
    }
}
