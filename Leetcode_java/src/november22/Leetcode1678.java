package november22;

public class Leetcode1678 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1678 obj = new Leetcode1678();
		String command = "G()(al)";
		String res = obj.interpret(command);
		System.out.println(res);
	}
    public String interpret(String command) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < command.length(); i++) {
            if (command.charAt(i) == 'G') {
                res.append("G");
            } else if (command.charAt(i) == '(') {
                if (command.charAt(i + 1) == ')') {
                    res.append("o");
                } else {
                    res.append("al");
                }
            }
        }
        return res.toString();
    }
}
