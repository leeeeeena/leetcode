package october22;

import java.util.ArrayList;
import java.util.List;

public class Leetcode1790 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1790 obj = new Leetcode1790();
		String s1 = "bank", s2 = "kanb";
		boolean res = obj.areAlmostEqual(s1, s2);
		System.out.println(res);
	}
	public boolean areAlmostEqual(String s1, String s2) {
        int n = s1.length();
        List<Integer> diff = new ArrayList<Integer>();
        for (int i = 0; i < n; ++i) {
            if (s1.charAt(i) != s2.charAt(i)) {
                if (diff.size() >= 2) {
                    return false;
                }
                diff.add(i);
            }
        }
        if (diff.isEmpty()) {
            return true;
        }
        if (diff.size() != 2) {
            return false;
        }
        return s1.charAt(diff.get(0)) == s2.charAt(diff.get(1)) && s1.charAt(diff.get(1)) == s2.charAt(diff.get(0));
    }
}
