package october22;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Leetcode811 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode811 obj = new Leetcode811();
		String[] cpdomains = {"9001 discuss.leetcode.com"};
		List<String> res = obj.subdomainVisits(cpdomains);
	}
	public List<String> subdomainVisits(String[] cpdomains) {
        Map<String,Integer> maps = new HashMap<String,Integer>();
        for(int i=0;i<cpdomains.length;i++){
            String this_cpdomain = cpdomains[i];
            int count = Integer.valueOf(this_cpdomain.split(" ")[0]);
            String domain1 = this_cpdomain.split(" ")[1];
            maps.put(domain1,maps.getOrDefault(domain1,0)+count);
            
            
            for(int j=0;j<domain1.length();j++) {
            	if(domain1.charAt(j)=='.') {
            		String sub = domain1.substring(j+1);
            		maps.put(sub,maps.getOrDefault(sub,0)+count);
            	}
            }
        }
        List<String> res = new ArrayList<String>();
        for(Map.Entry<String,Integer> m:maps.entrySet()){
            String cur = String.valueOf(m.getValue())+" "+m.getKey();
            res.add(cur);
        }
        return res;
	}
}
