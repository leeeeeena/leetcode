package october22;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class Leetcode856 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode856 obj = new Leetcode856();
		String s = "(())";
		int res = obj.scoreOfParentheses(s);
		System.out.println(res);
	}
    public int scoreOfParentheses(String s) {
    	Deque<Integer> st = new ArrayDeque<Integer>();
    	st.push(0);
    	for (int i = 0; i < s.length(); i++) {
    		if(s.charAt(i)=='(') {
    			st.push(0);
    		}else {
    			int v = st.pop();
    			int top = st.pop() + Math.max(2 * v, 1);
                st.push(top);
    		}
    	}
    	return st.peek();
    }
}
