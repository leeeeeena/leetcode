package october22;

import java.util.ArrayDeque;
import java.util.Deque;

public class Leetcode862 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode862 obj = new Leetcode862();
		int[] nums = {2,-1,2};
		int k = 3;
		int res = obj.shortestSubarray(nums, k);
		System.out.println(res);
		
	}
    public int shortestSubarray(int[] nums, int k) {
    	int n = nums.length;
    	long[] preSumArr = new long[n+1];
    	for(int i=0;i<n;i++) {
    		preSumArr[i+1]=preSumArr[i]+nums[i];
    	}
    	int res = n+1;
    	Deque<Integer> queue = new ArrayDeque<Integer>();
    	for(int i=0;i<=n;i++) {
    		long curSum = preSumArr[i];
    		while(!queue.isEmpty() && curSum-preSumArr[queue.peekFirst()]>=k) {
    			res = Math.min(res, i-queue.pollFirst());
    		}
    		while(!queue.isEmpty() && preSumArr[queue.peekLast()]>=curSum) {
    			queue.pollLast();
    		}
    		queue.offerLast(i);
    	}
    	return res<n+1?res:-1;
    }
}
