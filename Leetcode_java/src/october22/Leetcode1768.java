package october22;

public class Leetcode1768 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1768 obj = new Leetcode1768();
		String word1 = "ab", word2 = "pqrs";
		String res = obj.mergeAlternately(word1, word2);
		System.out.println(res);
	}
    public String mergeAlternately(String word1, String word2) {
    	int n1 = word1.length();
    	int n2 = word2.length();
    	
    	char[] res = new char[n1+n2];
    	int idx = 0;
    	int i1=0,i2=0;
    	while(i1<n1 &&i2<n2) {
    		res[idx++]=word1.charAt(i1++);

    		res[idx++]=word2.charAt(i2++);

    	}
    	while(i1<n1) {
    		res[idx++]=word1.charAt(i1++);
    	}
    	while(i2<n2) {
    		res[idx++]=word2.charAt(i2++);
    	}
    	return String.valueOf(res);
    }
}
