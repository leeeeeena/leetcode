package october22;

import java.util.Arrays;

public class Leetcode870 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode870 obj = new Leetcode870();
		int[] nums1 = {2,7,11,15};
		int[] nums2 = {1,10,4,11};
		int[] res = obj.advantageCount(nums1, nums2);
		for(int i=0;i<res.length;i++) {
			System.out.println(res[i]);
		}
	}
    public int[] advantageCount(int[] nums1, int[] nums2) {
    	int n = nums1.length;
    	Integer[] idx1 = new Integer[n];
    	Integer[] idx2 = new Integer[n];
    	for(int i=0;i<n;i++) {
    		idx1[i]=i;
    		idx2[i]=i;
    	}
    	
    	Arrays.sort(idx1,(a,b)->(nums1[a]-nums1[b]));
    	Arrays.sort(idx2,(a,b)->(nums2[a]-nums2[b]));
    	
    	int[] ans = new int[n];
    	int left = 0;
    	int right = n-1;
    	for(int i=0;i<n;i++) {
    		if(nums1[idx1[i]]>nums2[idx2[left]]) {
    			ans[idx2[left]]=nums1[idx1[i]];
    			left++;
    		}else {
    			ans[idx2[right]]=nums1[idx1[i]];
    			right--;
    		}
    	}
    	return ans;
    }
}
