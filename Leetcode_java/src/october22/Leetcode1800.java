package october22;

public class Leetcode1800 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1800 obj = new Leetcode1800();
		int[] nums = {10,20,30,5,10,50};
		int res = obj.maxAscendingSum(nums);
		System.out.println(res);
	}
    public int maxAscendingSum(int[] nums) {
        int n = nums.length;
        int pre = nums[0];
        int sum = pre;
        int max = sum;
        for(int i=1;i<n;i++) {
        	if(nums[i]>pre) {
        		sum+=nums[i];
        		max = Math.max(max, sum);
        		pre = nums[i];
        	}else {
        		pre = nums[i];
        		sum = pre;
        		max = Math.max(max, sum);
        	}
        }
        return max;
    }
}
