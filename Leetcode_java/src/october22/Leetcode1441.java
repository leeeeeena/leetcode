package october22;

import java.util.ArrayList;
import java.util.List;

public class Leetcode1441 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Leetcode1441 obj = new Leetcode1441();
		int[] target = {1,3};
		int n = 3;
		List<String> res = obj.buildArray(target, n);
		for(int i=0;i<res.size();i++) {
			System.out.println(res.get(i));
		}
	}
    public List<String> buildArray(int[] target, int n) {
        List<String> res = new ArrayList<String>();
    	int left = 0;
    	for(int i=1;i<=n;i++) {
    		res.add("Push");

    		if( target[left]!=i) {
    			res.add("Pop");
    		}else{
    			left++;
    		}
            
            if(left>=target.length){
                break;
            }
    		
    	}
    	return res;
    }
}
